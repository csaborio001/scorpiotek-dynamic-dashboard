<?php 

/*  	
    Plugin Name: ScorpioTek Dynamic Dashboard
    Description: Displays Dashboard Widgets from PHP Foles
    @since  1.0
    Version: 1.3
	Text Domain: scorpiotek.com
*/

function remove_dashboard_widgets() {
      // Remove All Dashboard Widgets.
      remove_action('welcome_panel', 'wp_welcome_panel');     
      remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
      remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );  
      remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); 
      remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  
      remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   
      remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  
      remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  
      remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
      remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
}
 
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

function add_custom_dashboard_widget_content() {
	
    $dir = __DIR__ . '/dashboard-widgets';
    $dashboard_files = glob( $dir . '/*.php');
    foreach ($dashboard_files as $dashboard_file) {
        $widget_filename = basename($dashboard_file);
        // Widget Slug is just the filename without the php extension	
        $widget_slug = basename($dashboard_file, '.php');
        // Convert filename (for example support-contact) to "Support Contact" for a suitable widget title
        $widget_title = str_replace('-', ' ', ucwords(basename($dashboard_file, '.php'),'-'));
        wp_add_dashboard_widget( $widget_id = $widget_slug,
                                 $widget_name = $widget_title,
                                 $callback = function($var, $args) {
                                    include ('dashboard-widgets/' . $args['args']);
                                 },
                                 $control_callback = null,
                                 $callback_args = basename($dashboard_file)
        );			
    }
}
add_action( 'wp_dashboard_setup', 'add_custom_dashboard_widget_content' );

?>